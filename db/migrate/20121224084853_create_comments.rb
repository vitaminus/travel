class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :commenter
      t.text :body
      t.references :country

      t.timestamps
    end
    add_index :comments, :country_id
  end
end
