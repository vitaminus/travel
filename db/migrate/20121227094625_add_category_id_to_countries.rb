class AddCategoryIdToCountries < ActiveRecord::Migration
  def change
  	add_column :countries, :category_id, :integer
  end
end
