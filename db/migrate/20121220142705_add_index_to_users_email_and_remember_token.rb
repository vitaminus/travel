class AddIndexToUsersEmailAndRememberToken < ActiveRecord::Migration
  def change
  	add_index( :users, [ :email, :name ], unique: true)
  	add_index :users, :remember_token
  end
end
