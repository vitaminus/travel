# encoding: UTF-8
class Category < ActiveRecord::Base
  extend FriendlyId
	
  attr_accessible :slug, :title, :url_title
  friendly_id :url_title, use: :slugged
  has_many :countries

  VALID_URL_TITLE = /[A-Za-z]/i

  validates :title, presence: true
  validates :url_title, presence: true,
            format: { with: VALID_URL_TITLE, message: "Только английскими буквами" }, 
            uniqueness: true

  default_scope order: 'categories.title ASC'

end
