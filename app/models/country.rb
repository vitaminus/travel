# encoding: UTF-8
class Country < ActiveRecord::Base
  extend FriendlyId
  friendly_id :url_name, use: :slugged
  has_many :comments, dependent: :destroy
  belongs_to :category, :foreign_key => 'category_id'

  attr_accessible :content, :url_name, :title, :category_id
  
  VALID_URL_NAME = /[A-Za-z]/i

  validates :title ,    presence: true, 
                        length: { minimum: 3, maximum: 50 },
                        uniqueness: true
  validates :url_name,  presence: true, 
                        length: { minimum: 4, maximum: 25 },
                        format: { with: VALID_URL_NAME, message: "Только английскими буквами" }, 
                        uniqueness: true
  validates :content,   presence: true,
                        length: { maximum: 12000 }

  default_scope order: 'countries.title ASC'
end
