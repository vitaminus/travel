# encoding: UTF-8
class Message < ActiveRecord::Base
  attr_accessible :content, :email, :name

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  validates :name ,   presence: true, 
                      length: { minimum: 3, maximum: 25 }
  validates :email,   presence: true, 
                      format: { with: VALID_EMAIL_REGEX, message: "Неправильный формат email" }
  validates :content, presence: true,
                      length: { maximum: 500 }
end