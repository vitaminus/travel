class Comment < ActiveRecord::Base
  belongs_to :country
  attr_accessible :body, :commenter

  validates :commenter, presence: true, length: { maximum: 25 }
  validates :body, presence: true, length: { maximum: 500 }
end
