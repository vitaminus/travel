# encoding: UTF-8
class CategoriesController < ApplicationController
  before_filter :categories, only: [:index,:show]
  before_filter :category, only: [:show, :edit, :update, :destroy]

  def index
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(params[:category])
    if @category.save
      flash[:success] = "Категория успешно добавлена"
      redirect_to new_category_path
    else
      render 'new'
    end
  end

  def show
    @countries = Country.where("category_id = ?", @category.id)
  end

  def edit
  end

  def update
    if @category.update_attributes(params[:category])
      flash[:success] = "Категория успешно изменена"
      redirect_to category_path(@category)
    else
      render 'edit'
    end
  end

  def destroy
    @category.destroy
    flash[:success] = "Категория успешно удалена"
    redirect_to category_path
  end
end
