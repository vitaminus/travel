# encoding: UTF-8
class MessagesController < ApplicationController
  
  def new
    @message = Message.new
  end

  def create
    @message = Message.new(params[:message])
    if @message.save
      flash[:success] = "Ваше сообщение отправлено. Спасибо за отзыв."
      redirect_to contact_path
    else
      render 'new'
    end
  end

  def index
    @messages = Message.all
  end

end
