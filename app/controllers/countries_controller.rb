# encoding: UTF-8
class CountriesController < ApplicationController
  before_filter :signed_in_user,  only: [:new, :create, :edit, :destroy]
  before_filter :country,         only: [:show, :edit, :update, :destroy]
  before_filter :categories
  #add_breadcrumb "Главная", :root_path
  
  def new
    @country = Country.new
  end

  def create
    @country = Country.new(params[:country])
    
    if @country.save
      flash[:success] = "Страна успешно добавлена"
      redirect_to country_path(@country)
    else
      render 'new'
    end
  end
  
  def show
    #add_breadcrumb "#{@country.title}", :country_path
  end
  
  def index
    countries
  end
  
  def edit
  end
  
  def update
    if @country.update_attributes(params[:country])
      flash[:success] = "Обновление прошло успешно"
      redirect_to country_path(@country)
    else
      render 'edit'
    end
  end

  def destroy
    @country.destroy
    flash[:success] = "Страна удалена"
    redirect_to :action => :index
  end
end
