class ApplicationController < ActionController::Base
  protect_from_forgery
  include SessionsHelper
  include CountriesHelper
  include CategoriesHelper

  before_filter :categories
  before_filter :countries

  protected

    def categories
      @categories = Category.all
    end

    def countries
      @countries = Country.all
    end
    
end
