# encoding: UTF-8
class CommentsController < ApplicationController

  def create
    @country = Country.find(params[:country_id])
    @comment = @country.comments.create(params[:comment])
    if @comment.save
      flash[:success] = "Комментарий добавлен."
      redirect_to country_path(@country)
    else
      flash[:error] = "Комментарий не добавлен"
      redirect_to country_path(@country)
    end
  end

  def destroy
    @country = Country.find(params[:country_id])
    @comment = @country.comments.find(params[:id])
    @comment.destroy
    redirect_to country_path(@country), notice: "Комментарий удален"
  end

end
